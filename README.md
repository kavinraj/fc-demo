# Firesec

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Development server

Run `ng serve && npm run server` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## TODO

[X] ~~URL added in all page~~

[X] ~~Modal added dynamically~~

[ ] Date Picker need to add style

[ ] Add Asset form need to validate the form with touch

[ ] Pagination need to update onChange search input

[ ] Need to work CDK drag and drop

## Administration overview

[ ] Multiple router need to enable

[ ] Pagination for Event log in Overview

[ ] Checkbox click enable the delete in manage users in overview

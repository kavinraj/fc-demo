import { Subject, observable } from 'rxjs';

export class AssetList {
  repodata = [
    {
      "assetId": 87,
      "assetName": "CiscoDemoTest",
      "assetType": "Firewall",
      "createdOn": "2020-07-09T07:05:46.197",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 84,
      "assetName": "CIscoManual2",
      "assetType": "Firewall",
      "createdOn": "2020-07-06T05:01:47.66",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 83,
      "assetName": "CIscoManual1",
      "assetType": "Firewall",
      "createdOn": "2020-07-06T04:59:18.833",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 81,
      "assetName": "CIscoManual",
      "assetType": "Firewall",
      "createdOn": "2020-07-06T04:57:00.177",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 80,
      "assetName": "Cisco234",
      "assetType": "Firewall",
      "createdOn": "2020-07-01T12:34:03.84",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 79,
      "assetName": "CIsco23",
      "assetType": "Firewall",
      "createdOn": "2020-07-01T11:30:24.413",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 78,
      "assetName": "FortigateDemo",
      "assetType": "Firewall",
      "createdOn": "2020-06-30T12:07:19.883",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 76,
      "assetName": "CiscoDemo",
      "assetType": "Firewall",
      "createdOn": "2020-06-30T11:40:11.893",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 75,
      "assetName": "Cisco2",
      "assetType": "Firewall",
      "createdOn": "2020-06-25T10:20:28.07",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 73,
      "assetName": "Cisco1",
      "assetType": "Firewall",
      "createdOn": "2020-06-25T05:17:26.76",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 70,
      "assetName": "AAAA",
      "assetType": "Firewall",
      "createdOn": "2020-06-24T11:47:11.183",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 69,
      "assetName": "AAAA",
      "assetType": "Firewall",
      "createdOn": "2020-06-24T11:26:19.103",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 68,
      "assetName": "Sonicwall",
      "assetType": "Firewall",
      "createdOn": "2020-06-24T09:04:33.557",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 67,
      "assetName": "Sophos",
      "assetType": "Firewall",
      "createdOn": "2020-06-24T08:27:59.523",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 66,
      "assetName": "CyberRoam",
      "assetType": "Firewall",
      "createdOn": "2020-06-24T08:23:46.537",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 65,
      "assetName": "PaloAlto",
      "assetType": "Firewall",
      "createdOn": "2020-06-24T08:21:16.783",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 64,
      "assetName": "CheckPoint",
      "assetType": "Firewall",
      "createdOn": "2020-06-24T06:48:53.233",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 58,
      "assetName": "Juniper",
      "assetType": "Firewall",
      "createdOn": "2020-06-23T11:46:00.243",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 52,
      "assetName": "Cisco1",
      "assetType": "Firewall",
      "createdOn": "2020-06-23T11:09:45.383",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 51,
      "assetName": "FortigateConf",
      "assetType": "Firewall",
      "createdOn": "2020-06-23T07:47:19.157",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 48,
      "assetName": "FortigateConf",
      "assetType": "Firewall",
      "createdOn": "2020-06-23T07:27:03.18",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 47,
      "assetName": "FortigateConf",
      "assetType": "Firewall",
      "createdOn": "2020-06-23T07:21:54.813",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 46,
      "assetName": "FortigateConf",
      "assetType": "Firewall",
      "createdOn": "2020-06-23T06:39:28.93",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 45,
      "assetName": "FortigateConf",
      "assetType": "Firewall",
      "createdOn": "2020-06-23T05:16:16.533",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 44,
      "assetName": "asdsadfsdfsdf",
      "assetType": "Firewall",
      "createdOn": "2020-06-23T04:51:17.58",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 43,
      "assetName": "asdsadfsdfsdf",
      "assetType": "Firewall",
      "createdOn": "2020-06-23T04:48:43.053",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 37,
      "assetName": "asdsadfsdfsdf",
      "assetType": "Firewall",
      "createdOn": "2020-06-22T10:55:31.357",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 36,
      "assetName": "asdsadfsdfsdf",
      "assetType": "Firewall",
      "createdOn": "2020-06-22T10:48:51.737",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 35,
      "assetName": "asdsadfsdfsdf",
      "assetType": "Firewall",
      "createdOn": "2020-06-22T10:47:52.613",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 34,
      "assetName": "asdsadfsdfsdf",
      "assetType": "Firewall",
      "createdOn": "2020-06-22T10:47:44.167",
      "owner": "Manik Salunkhe"
  },
  {
      "assetId": 32,
      "assetName": "asdsadfsdfsdf",
      "assetType": "Firewall",
      "createdOn": "2020-06-22T07:14:10.077",
      "owner": "Manik Salunkhe"
  }
  ];

  obs = new Subject();
  fetchData() {
    setTimeout(() => {
      this.obs.next({ repodata: this.repodata });
    }, 2000);
    return this.obs;
  }
}

import { Subject, observable } from 'rxjs';

export class RuleBase {
  RuleBaseData = [
    {
      "id": 0,
      "name": "vendor_access_in",
      "in_interface": "vendor",
      "out_interface": "",
      "source": [
        "DM_INLINE_NETWORK_1",
          "68.188.108.21",
          "DM_INLINE_NETWORK_2"
      ],
      "destination": "ind-sap03_p",
      "service_protocol": 3299,
      "action": "permit",
      "status": "disable",
      "log_traffic": "disable",
      "exception": "temporary",
      "rule_type": ""
  },
  {
      "id": 1,
      "name": "outside_cryptomap",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "Any"
      ],
      "destination": "ind-sap03_p",
      "service_protocol": "DM_IN_SRV_1",
      "action": "deny",
      "status": "enabled",
      "log_traffic": "disable",
      "exception": "permenant",
      "rule_type": ""
  },
  {
      "id": 2,
      "name": "outside_access_in",
      "in_interface": "vendor",
      "out_interface": "",
      "source": [
          "DM_INLINE_NETWORK_2",
          "68.188.108.21"
      ],
      "destination": "ind-sap03_p",
      "service_protocol": 3299,
      "action": "permit",
      "status": "enabled",
      "log_traffic": "enabled",
      "exception": "none",
      "rule_type": "extended"
  },
  {
      "id": 3,
      "name": "outside_cryptomap_1",
      "in_interface": "vendor",
      "out_interface": "",
      "source": [
          "Any",
          "68.188.108.21"
      ],
      "destination": "Any",
      "service_protocol": "icmp",
      "action": "permit",
      "status": "disable",
      "log_traffic": "enabled",
      "exception": "none",
      "rule_type": ""
  },
  {
      "id": 4,
      "name": "outside_cryptomap",
      "in_interface": "outside",
      "out_interface": "",
      "source": [
          "68.188.108.21",
          "68.188.108.21"
      ],
      "destination": "Any",
      "service_protocol": "DIS2",
      "action": "deny",
      "status": "enabled",
      "log_traffic": "enabled",
      "exception": "none",
      "rule_type": "extended"
  },
  {
      "id": 5,
      "name": "outside_cryptomap",
      "in_interface": "vendor",
      "out_interface": "",
      "source": [
          "Any"
      ],
      "destination": "Any",
      "service_protocol": 3299,
      "action": "permit",
      "status": "enabled",
      "log_traffic": "enabled",
      "exception": "none",
      "rule_type": ""
  },
  {
      "id": 6,
      "name": "vendor_access_in",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "DM_INLINE_NETWORK_2",
          "Any"
      ],
      "destination": "OV",
      "service_protocol": 3299,
      "action": "permit",
      "status": "enabled",
      "log_traffic": "enabled",
      "exception": "temporary",
      "rule_type": ""
  },
  {
      "id": 7,
      "name": "outside_access_in",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "Any",
          "DM_INLINE_NETWORK_2"
      ],
      "destination": "Any",
      "service_protocol": "DIS2",
      "action": "permit",
      "status": "disable",
      "log_traffic": "disable",
      "exception": "permenant",
      "rule_type": ""
  },
  {
      "id": 8,
      "name": "outside_access_in",
      "in_interface": "outside",
      "out_interface": "",
      "source": [
          "DM_INLINE_NETWORK_2",
          "vendor_subnet"
      ],
      "destination": "fp",
      "service_protocol": 3299,
      "action": "deny",
      "status": "disable",
      "log_traffic": "enabled",
      "exception": "permenant",
      "rule_type": ""
  },
  {
      "id": 9,
      "name": "outside_access_in",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "Any",
          "vendor_subnet"
      ],
      "destination": "OV",
      "service_protocol": "icmp",
      "action": "permit",
      "status": "enabled",
      "log_traffic": "disable",
      "exception": "permenant",
      "rule_type": "extended"
  },
  {
      "id": 10,
      "name": "outside_cryptomap",
      "in_interface": "outside",
      "out_interface": "",
      "source": [
          "FP",
          "68.188.108.21"
      ],
      "destination": "dns_servers",
      "service_protocol": "DIS2",
      "action": "permit",
      "status": "disable",
      "log_traffic": "disable",
      "exception": "temporary",
      "rule_type": ""
  },
  {
      "id": 11,
      "name": "outside_cryptomap_1",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "68.188.108.21",
          "Any"
      ],
      "destination": "OV",
      "service_protocol": "DIS2",
      "action": "permit",
      "status": "disable",
      "log_traffic": "disable",
      "exception": "none",
      "rule_type": "extended"
  },
  {
      "id": 12,
      "name": "outside_access_in",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "68.188.108.21",
          "DM_INLINE_NETWORK_2"
      ],
      "destination": "Any",
      "service_protocol": "DIS2",
      "action": "deny",
      "status": "enabled",
      "log_traffic": "enabled",
      "exception": "temporary",
      "rule_type": ""
  },
  {
      "id": 13,
      "name": "outside_access_in",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "vendor_subnet",
          "DM_INLINE_NETWORK_2"
      ],
      "destination": "dns_servers",
      "service_protocol": "DIS2",
      "action": "permit",
      "status": "disable",
      "log_traffic": "disable",
      "exception": "none",
      "rule_type": ""
  },
  {
      "id": 14,
      "name": "outside_access_in",
      "in_interface": "vendor",
      "out_interface": "",
      "source": [
          "68.188.108.21",
          "Any"
      ],
      "destination": "vpn_ri",
      "service_protocol": 3299,
      "action": "deny",
      "status": "enabled",
      "log_traffic": "enabled",
      "exception": "permenant",
      "rule_type": ""
  },
  {
      "id": 15,
      "name": "outside_access_in",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "Any"
      ],
      "destination": "ind-sap03_p",
      "service_protocol": "icmp",
      "action": "permit",
      "status": "enabled",
      "log_traffic": "enabled",
      "exception": "temporary",
      "rule_type": ""
  },
  {
      "id": 16,
      "name": "outside_cryptomap",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "68.188.108.21",
          "68.188.108.21"
      ],
      "destination": "fp",
      "service_protocol": "DM_IN_SRV_1",
      "action": "deny",
      "status": "enabled",
      "log_traffic": "disable",
      "exception": "none",
      "rule_type": ""
  },
  {
      "id": 17,
      "name": "outside_cryptomap_1",
      "in_interface": "outside",
      "out_interface": "",
      "source": [
          "FP",
          "68.188.108.21"
      ],
      "destination": "fp",
      "service_protocol": "icmp",
      "action": "permit",
      "status": "disable",
      "log_traffic": "disable",
      "exception": "permenant",
      "rule_type": "extended"
  },
  {
      "id": 18,
      "name": "outside_cryptomap_1",
      "in_interface": " ",
      "out_interface": "",
      "source": [
          "DM_INLINE_NETWORK_2",
          "FP"
      ],
      "destination": "Any",
      "service_protocol": 3299,
      "action": "permit",
      "status": "disable",
      "log_traffic": "disable",
      "exception": "none",
      "rule_type": "extended"
  },
  {
      "id": 19,
      "name": "outside_access_in",
      "in_interface": "vendor",
      "out_interface": "",
      "source": [
          "DM_INLINE_NETWORK_2",
          "Any"
      ],
      "destination": "Any",
      "service_protocol": "ip",
      "action": "permit",
      "status": "disable",
      "log_traffic": "enabled",
      "exception": "temporary",
      "rule_type": ""
  }
  ];

  obs = new Subject();
  fetchData() {
    setTimeout(() => {
      this.obs.next({ RuleBaseData: this.RuleBaseData });
    }, 2000);
    return this.obs;
  }
}

import { Subject, observable } from 'rxjs';

export class LargeSubnet {
  LargeSubnetData = [

    {
      'id': 0,
      'host': 'outside_access_in',
      'service_protocol': 'DM_IN_SRV_1',
      'subnet_mask': '231.196.126.188'
    },
    {
      'id': 1,
      'host': 'outside_access_in',
      'service_protocol': 'DIS2',
      'subnet_mask': '113.245.244.152'
    },
    {
      'id': 2,
      'host': 'outside_access_in',
      'service_protocol': 'icmp',
      'subnet_mask': '133.233.238.174'
    },
    {
      'id': 3,
      'host': 'outside_cryptomap',
      'service_protocol': 'icmp',
      'subnet_mask': '156.240.114.167'
    },
    {
      'id': 4,
      'host': 'outside_cryptomap',
      'service_protocol': 'DM_IN_SRV_1',
      'subnet_mask': '135.167.144.182'
    },
    {
      'id': 5,
      'host': 'outside_cryptomap_1',
      'service_protocol': 'DM_IN_SRV_1',
      'subnet_mask': '126.222.199.118'
    },
    {
      'id': 6,
      'host': 'outside_access_in',
      'service_protocol': 3299,
      'subnet_mask': '108.119.177.152'
    },
    {
      'id': 7,
      'host': 'outside_cryptomap_1',
      'service_protocol': 'ip',
      'subnet_mask': '108.243.129.163'
    },
    {
      'id': 8,
      'host': 'outside_cryptomap_1',
      'service_protocol': 3299,
      'subnet_mask': '230.137.147.214'
    },
    {
      'id': 9,
      'host': 'outside_access_in',
      'service_protocol': 'ip',
      'subnet_mask': '102.178.121.133'
    },
    {
      'id': 10,
      'host': 'outside_access_in',
      'service_protocol': 'icmp',
      'subnet_mask': '172.128.152.200'
    },
    {
      'id': 11,
      'host': 'vendor_access_in',
      'service_protocol': 'icmp',
      'subnet_mask': '221.181.133.114'
    },
    {
      'id': 12,
      'host': 'outside_cryptomap_1',
      'service_protocol': 'DIS2',
      'subnet_mask': '191.181.235.148'
    },
    {
      'id': 13,
      'host': 'outside_cryptomap',
      'service_protocol': 'DIS2',
      'subnet_mask': '113.104.186.223'
    },
    {
      'id': 14,
      'host': 'outside_access_in',
      'service_protocol': 'ip',
      'subnet_mask': '201.235.203.181'
    },
    {
      'id': 15,
      'host': 'outside_access_in',
      'service_protocol': 'icmp',
      'subnet_mask': '119.191.201.103'
    },
    {
      'id': 16,
      'host': 'outside_access_in',
      'service_protocol': 'icmp',
      'subnet_mask': '100.225.145.212'
    },
    {
      'id': 17,
      'host': 'outside_cryptomap_1',
      'service_protocol': 'ip',
      'subnet_mask': '132.224.124.169'
    },
    {
      'id': 18,
      'host': 'outside_cryptomap',
      'service_protocol': 'DIS2',
      'subnet_mask': '117.105.187.138'
    },
    {
      'id': 19,
      'host': 'outside_cryptomap',
      'service_protocol': 3299,
      'subnet_mask': '161.170.164.112'
    }

  ];

  obs = new Subject();
  fetchData() {
    setTimeout(() => {
      this.obs.next({ LargeSubnetData: this.LargeSubnetData });
    }, 2000);
    return this.obs;
  }
}

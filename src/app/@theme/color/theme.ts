export interface Theme {
  name: string;
  properties: any;
}

export const light: Theme = {
  name: "light",
  properties: {
    '--base': '#f5f9ff',
    '--slate': '#3f5669'
  }
};

export const dark: Theme = {
  name: "dark",
  properties: {
    '--base': '#001c34',
    '--slate': '#c6e4fe'
  }
};

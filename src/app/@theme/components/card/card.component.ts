import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})

export class CardComponent implements OnInit {

  @Input() card_type: string;
  @Input() card_icon: boolean;
  @Input() icon_position: string;
  @Input() card_status: string;
  @Input() bg_color: string;
  @Input() text_color: string;

  @Input('asset') asset: any;

  isHovering: boolean = false;

  @HostListener('mouseenter') onMouseEnter() {
    this.isHovering = true;
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.isHovering = false;
  }

  readMores: boolean = false;

  constructor(private router: Router,
    private route: ActivatedRoute) {
  }
  private sub: any;
  private id: number;

  ngOnInit() {

    // this.asset.id.subscribe(id => console.log(id)); // Object {artistId: 12345}
    // console.log(this.asset.id); // Object {artistId: 12345}
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  pageLink() {
    if (this.asset.asset_type != "mgmt console") {
      console.log(' not mgt')
      this.router.navigate(['/assets/view/']);
    } else {
      this.router.navigate(['/assets/view/console']);
    }
  }
}

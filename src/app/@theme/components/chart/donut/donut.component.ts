import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import * as echarts from "echarts";

@Component({
  selector: "app-donut",
  templateUrl: "./donut.component.html",
  styleUrls: ["./donut.component.scss"],
})
export class DonutComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    // based on prepared DOM, initialize echarts instance
    // const myChart = echarts.init(document.getElementById('chartdonut'));
    const myChart: any = echarts.init(
      <HTMLInputElement>document.getElementById("chartdonut")
    );

    // specify chart configuration item and data
    const chart_option: any = {
      tooltip: {},
      legend: {
        orient: "horizontal",
        bottom: "0",
        data: [
          "Insecure rules",
          "Insecure service",
          "Redundant rules",
          "Shadow rules",
          "Disabled",
        ],
      },
      series: [
        {
          name: "Config Statistics",
          type: "pie",
          radius: ["50%", "80%"],
          avoidLabelOverlap: true,
          label: {
            normal: {
              show: false,
              position: "center",
            },
            emphasis: {
              show: false,
              textStyle: {
                fontSize: "30",
                fontWeight: "bold",
              },
            },
          },
          labelLine: {
            normal: {
              show: false,
            },
          },
          data: [
            { value: 335, name: "Insecure rules" },
            { value: 310, name: "Insecure service" },
            { value: 234, name: "Redundant rules" },
            { value: 135, name: "Shadow rules" },
            { value: 1548, name: "Disabled" },
          ],
        },
      ],
    };
    // use configuration item and data specified to show chart
    myChart.setOption(chart_option);
  }
}

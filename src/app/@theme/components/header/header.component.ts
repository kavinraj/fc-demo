import { Component, OnInit, HostListener } from "@angular/core";
import { ThemeService } from "../../color/theme.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  constructor(private themeService: ThemeService) {}

  ngOnInit() {
    const body = document.getElementsByTagName("body")[0];
    body.classList.add("theme--light");
  }

  // theme color toggle
  toggle() {
    if (this.themeService.isDarkTheme()) {
      this.themeService.setLightTheme();
    } else {
      this.themeService.setDarkTheme();
    }
  }

  hideRuleContent: boolean = false;
  toggles() {
    this.hideRuleContent = !this.hideRuleContent;
  }

  // Host listener
  // @HostListener("click", ["$event.target.id"]) onClick(id: any) {
  //   // select read more
  //   if (id == "user-actions") {
  //     this.hideRuleContent = true;
  //     console.log("true");
  //   } else {
  //     this.hideRuleContent = false;
  //     console.log("false");
  //   }
  // }
}

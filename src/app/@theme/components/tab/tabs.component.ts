/**
 * The main component that renders single TabComponent
 * instances.
 */

import {
  Component,
  ContentChildren,
  QueryList,
  AfterContentInit,
  ViewChild,
  ComponentFactoryResolver,
  ViewContainerRef,
} from "@angular/core";

import { TabComponent } from "./tab.component";

@Component({
  selector: "my-tabs",
  template: `
    <ul class="nav nav-tabs">
      <li
        *ngFor="let tab of tabs"
        (click)="selectTab(tab)"
        [class.active]="tab.active"
      >
        <span>{{ tab.title }}</span>
      </li>
    </ul>
    <ng-content></ng-content>
  `,
  styles: [
    `
      .nav.nav-tabs {
        display: flex;
        justify-content: space-between;
      }
      .nav.nav-tabs li {
        flex: 1 0 auto;
        margin-bottom: 30px;
      }
      .nav.nav-tabs li span {
        cursor: pointer;
        flex: 1 0 auto;
        display: block;
        font-family: var(--roboto-400);
        font-size: 16px;
        line-height: 19px;
        text-align: center;
        color: #6a8eac;
        position: relative;
        border-bottom: 1px solid rgba(1, 88, 162, 0.2);
        padding-bottom: 12px;
      }
      .nav.nav-tabs li span:after {
        content: "";
        content: none;
        display: inline-block;
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        height: 2px;
        background: #1393ff;
      }
      .nav.nav-tabs li:hover span,
      .nav.nav-tabs li.active span {
        color: #1393ff;
      }
      .nav.nav-tabs li.active span:after {
        content: "";
      }

      .tab-close {
        color: gray;
        text-align: right;
        cursor: pointer;
      }
    `,
  ],
})
export class TabsComponent implements AfterContentInit {
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  // contentChildren are set
  ngAfterContentInit() {
    // get all active tabs
    let activeTabs = this.tabs.filter((tab) => tab.active);

    // if there is no active tab set, activate the first
    if (activeTabs.length === 0) {
      this.selectTab(this.tabs.first);
    }
  }

  selectTab(tab: any) {
    // deactivate all tabs
    this.tabs.toArray().forEach((tab) => (tab.active = false));

    // activate the tab the user has clicked on.
    tab.active = true;
  }
}

import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-toaster",
  templateUrl: "./toaster.component.html",
  styleUrls: ["./toaster.component.scss"],
})
export class ToasterComponent implements OnInit {
  @Input() icon_position: string;
  @Input() card_type: string;

  superhero = this.card_type;

  constructor() {}

  ngOnInit() {}
}

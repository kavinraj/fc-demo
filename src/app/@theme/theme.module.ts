import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ThemeService } from "./color/theme.service";
import { RouterModule } from "@angular/router";

import {
  HeaderComponent,
  FooterComponent,
  CardComponent,
  ButtonComponent,
  ToasterComponent,
  TabComponent,
  TabsComponent,
  IconComponent,
  DonutComponent,
  ModalComponent,
  TooltipComponent,
} from "./components";

import { BackDirective } from "./directive/back.directive";
import { TooltipDirective } from "./components/tooltip/tooltip.directive";

const COMPONENTS = [
  HeaderComponent,
  FooterComponent,
  CardComponent,
  ButtonComponent,
  ToasterComponent,
  TabComponent,
  TabsComponent,
  IconComponent,
  DonutComponent,
  ModalComponent,
  TooltipComponent,
];

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [CommonModule, ...COMPONENTS],
  declarations: [...COMPONENTS, BackDirective, TooltipDirective],

  providers: [ThemeService],
})
export class ThemeModule {}

import { Component, Renderer2 } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `
    <div>
      <app-header> </app-header>
    </div>
    <div id='page-wrapper'>
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {
  previousUrl: string;

  constructor(private renderer: Renderer2, private router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.previousUrl) {
          this.renderer.removeClass(
            document.getElementById('page-wrapper'),
            this.previousUrl
          );
        }
        let currentUrlSlug = event.url.slice(1);
        if (currentUrlSlug) {
          this.renderer.addClass(
            document.getElementById('page-wrapper'),
            currentUrlSlug
          );
        }
        this.previousUrl = currentUrlSlug;
      }
    });
  }
  ngOnInit() {}
}

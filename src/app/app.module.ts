import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { MatTooltipModule } from '@angular/material/tooltip';
import { OverlayModule } from '@angular/cdk/overlay';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatInputModule,
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { PagesModule } from './pages/page.module';
import { ThemeModule } from './@theme/theme.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    ScrollingModule,
    ScrollDispatchModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule,
    BrowserModule,
    OverlayModule,
    FormsModule,
    ReactiveFormsModule,
    MonacoEditorModule.forRoot(),
    HttpClientModule,
    PagesModule,
    ThemeModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

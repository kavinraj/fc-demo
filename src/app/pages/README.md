{
    path: 'base/detail',
    component: RuleDetailComponent,
  },


  {
    path: 'compliance-policy',
    component: CompliancePolicyComponent,
  },
  {
    path: 'policy-rule',
    component: PolicyRuleComponent,
  },
  {
    path: 'report',
    component: GenerateComponent,
  },
  {
    path: 'change-management',
    component: ChangeManagementComponent,
  },
  {
    path: 'whatif',
    component: WhatifComponent,
  },
  {
    path: 'whatif/rule',
    component: RuleImpactComponent,
  },
  {
    path: 'whatif/shadow',
    component: ShadowRuleComponent,
  },
  {
    path: 'review',
    component: ReviewComponent,
  },




import { ReviewComponent } from './assets/rule/review/review.component';
import { RuleDetailComponent } from './assets/rule/rule-base/detail/detail.component';
import { CompliancePolicyComponent } from './assets/rule/compliance-policy/compliance-policy.component';
import { WhatifComponent } from './assets/rule/rule-base/whatif/whatif.component';
import { RuleImpactComponent } from './assets/rule/rule-base/whatif/rule-impact/rule-impact.component';
import { ShadowRuleComponent } from './assets/rule/rule-base/whatif/shadow-rule/shadow-rules.component';
import { ChangeManagementComponent } from './assets/rule/rule-base/change-management/change-management.component';
import { PolicyRuleComponent } from './assets/rule/compliance-policy/policy-rule/policy-rule.component';
import { GenerateComponent } from './assets/rule/report/generate/generate.component';
import { AdministrationComponent } from './administration/administration.component';
import { AdminOverviewComponent } from './administration/overview/overview.component';
import { ClientComponent } from './administration/client/client.component';
import { LicenseComponent } from './administration/license/license.component';
import { SettingComponent } from './administration/setting/setting.component';
import { UsersComponent } from './administration/users/users.component';
import { EventLogComponent } from './administration/event-log/event-log.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';

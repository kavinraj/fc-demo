import { Component, OnInit, ViewEncapsulation } from "@angular/core";

@Component({
  selector: "app-client",
  templateUrl: "./client.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./client.component.scss"],
})
export class ClientComponent implements OnInit {
  COMPARE_COUNT: number = 0;
  checkme(event: any) {
    const fileCompareButton = document.getElementById("fileCompare"),
      addAssetButton = document.getElementById("addAsset");

    if (event.target.checked) {
      this.COMPARE_COUNT++;
    } else {
      this.COMPARE_COUNT--;
    }

    if (this.COMPARE_COUNT >= 1) {
      fileCompareButton.classList.remove("d-none");
      addAssetButton.classList.add("d-none");
    } else {
      fileCompareButton.classList.add("d-none");
      addAssetButton.classList.remove("d-none");
    }
  }
  constructor() {}

  ngOnInit() {}
}

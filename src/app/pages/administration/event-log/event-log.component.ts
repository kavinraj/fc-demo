import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDatepickerModule } from "@angular/material/datepicker";

@Component({
  selector: "app-event-log",
  templateUrl: "./event-log.component.html",
  styleUrls: ["./event-log.component.scss"],
})
export class EventLogComponent implements OnInit {
  matDatepicker: Date;
  picker: any;

  date = new FormControl(new Date());

  start_date = new FormControl(new Date().toISOString());
  end_date = new FormControl(new Date().toISOString());

  constructor() {}

  ngOnInit() {}
}

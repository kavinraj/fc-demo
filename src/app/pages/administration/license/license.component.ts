import { Component, OnInit, ViewEncapsulation } from "@angular/core";

@Component({
  selector: "app-license",
  templateUrl: "./license.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./license.component.scss"],
})
export class LicenseComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

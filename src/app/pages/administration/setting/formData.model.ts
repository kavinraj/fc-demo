export class smptData {
  smptServer: String;
  smptUsername: String;
  smptPassword: String;
  smptSSL: String;
  smptTimeout: String;
  smptSendername: String;
  smptEmailRemainderDays: String;
  smptRemainderLimit: String;
}

export class activeDirectoryFormData {
  activeDirectoryDomainName: String;
  activeDirectorDomainUserName: String;
  activeDirectorDomainPassword: String;
}

export class changeManagementFormData {
  changeManagementSiteTool: String;
  changeManagementUrl: String;
  changeManagementAuthenticationType: String;
  changeManagementUserName: String;
  changeManagementPassword: String;
}

export class siteSettingsFormData {
  siteSettingsiteURL: String;
}

export class continuousComplianceFormData {
  continuousComplianceLogRetention: number;
}

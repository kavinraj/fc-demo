import {
  Component,
  ViewEncapsulation,
  HostListener,
  ElementRef,
  ViewChild,
  Renderer2,
} from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { smptData } from "./formData.model";
import { activeDirectoryFormData } from "./formData.model";
import { changeManagementFormData } from "./formData.model";
import { siteSettingsFormData } from "./formData.model";
import { continuousComplianceFormData } from "./formData.model";

@Component({
  selector: "app-setting",
  templateUrl: "./setting.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./setting.component.scss"],
})
export class SettingComponent {
  smptFormsubmitted = false;
  activeDirectoryFormsubmitted = false;
  changeManagementFormsubmitted = false;
  siteSettingFormsubmitted = false;
  continuousComplianceFormsubmitted = false;

  smptForm: FormGroup;
  activeDirectoryForm: FormGroup;
  changeManagementForm: FormGroup;
  siteSettingsForm: FormGroup;
  continuousComplianceForm: FormGroup;

  smptFormData: smptData = {
    smptServer: "",
    smptUsername: "",
    smptPassword: "",
    smptSSL: "",
    smptTimeout: "",
    smptSendername: "",
    smptEmailRemainderDays: "",
    smptRemainderLimit: "",
  };

  activeDirectoryFormData: activeDirectoryFormData = {
    activeDirectoryDomainName: "",
    activeDirectorDomainUserName: "",
    activeDirectorDomainPassword: "",
  };

  changeManagementFormData: changeManagementFormData = {
    changeManagementSiteTool: "",
    changeManagementUrl: "",
    changeManagementAuthenticationType: "",
    changeManagementUserName: "",
    changeManagementPassword: "",
  };

  siteSettingsFormData: siteSettingsFormData = {
    siteSettingsiteURL: "",
  };

  continuousComplianceFormData: continuousComplianceFormData = {
    continuousComplianceLogRetention: 90,
  };

  constructor(
    private router: Router,
    private elRef: ElementRef,
    private renderer: Renderer2,
    private fb: FormBuilder
  ) {
    this.smptFormCreate();
    this.activeDirectoryFormCreate();
    this.changeManagementFormCreate();
    this.siteSettingFormCreate();
    this.continuousComplianceFormCreate();
  }

  smptFormCreate() {
    this.smptForm = this.fb.group({
      smptServer: [this.smptFormData.smptServer, Validators.required],
      smptUsername: [this.smptFormData.smptUsername, Validators.required],
      smptPassword: [this.smptFormData.smptPassword, Validators.required],
      smptSSL: [this.smptFormData.smptSSL, Validators.required],
      smptTimeout: [this.smptFormData.smptTimeout, Validators.required],
      smptSendername: [this.smptFormData.smptSendername, Validators.required],
      smptEmailRemainderDays: [
        this.smptFormData.smptEmailRemainderDays,
        Validators.required,
      ],
      smptRemainderLimit: [
        this.smptFormData.smptRemainderLimit,
        Validators.required,
      ],
    });
  }

  activeDirectoryFormCreate() {
    this.activeDirectoryForm = this.fb.group({
      activeDirectoryDomainName: [
        this.activeDirectoryFormData.activeDirectoryDomainName,
        Validators.required,
      ],
      activeDirectorDomainUserName: [
        this.activeDirectoryFormData.activeDirectorDomainUserName,
        Validators.required,
      ],
      activeDirectorDomainPassword: [
        this.activeDirectoryFormData.activeDirectorDomainPassword,
        Validators.required,
      ],
    });
  }

  changeManagementFormCreate() {
    this.changeManagementForm = this.fb.group({
      changeManagementSiteTool: [
        this.changeManagementFormData.changeManagementSiteTool,
        Validators.required,
      ],
      changeManagementUrl: [
        this.changeManagementFormData.changeManagementUrl,
        Validators.required,
      ],
      changeManagementAuthenticationType: [
        this.changeManagementFormData.changeManagementAuthenticationType,
        Validators.required,
      ],
      changeManagementUserName: [
        this.changeManagementFormData.changeManagementUserName,
        Validators.required,
      ],
      changeManagementPassword: [
        this.changeManagementFormData.changeManagementPassword,
        Validators.required,
      ],
    });
  }

  siteSettingFormCreate() {
    this.siteSettingsForm = this.fb.group({
      siteSettingsiteURL: [
        this.siteSettingsFormData.siteSettingsiteURL,
        Validators.required,
      ],
    });
  }

  public counter: number = 90;

  increment() {
    this.counter += 1;
  }

  decrement() {
    if (this.counter > 1) {
      this.counter -= 1;
    }
  }

  continuousComplianceFormCreate() {
    this.continuousComplianceForm = this.fb.group({
      continuousComplianceLogRetention: [
        this.continuousComplianceFormData.continuousComplianceLogRetention,
        Validators.required,
      ],
    });
  }

  onsmptFormSubmit() {
    this.smptFormsubmitted = true;
    alert("SUCCESS!! :-)\n\n" + JSON.stringify(this.smptFormData, null, 4));
    console.warn(this.smptFormData);
  }

  onactiveDirectoryFormSubmit() {
    this.activeDirectoryFormsubmitted = true;
    alert(
      "SUCCESS!! :-)\n\n" +
        JSON.stringify(this.activeDirectoryFormData, null, 4)
    );
    console.warn(this.activeDirectoryFormData);
  }

  onchangeManagementFormSubmit() {
    this.changeManagementFormsubmitted = true;
    alert(
      "SUCCESS!! :-)\n\n" +
        JSON.stringify(this.changeManagementFormData, null, 4)
    );
    console.warn(this.changeManagementFormData);
  }

  onsiteSettingFromSubmit() {
    this.siteSettingFormsubmitted = true;
    alert(
      "SUCCESS!! :-)\n\n" + JSON.stringify(this.siteSettingsFormData, null, 4)
    );
    console.warn(this.siteSettingsFormData);
  }

  oncontinuousComplianceFormSubmit() {
    this.continuousComplianceFormsubmitted = true;
    alert(
      "SUCCESS!! :-)\n\n" +
        JSON.stringify(this.continuousComplianceFormData, null, 4)
    );
    console.warn(this.continuousComplianceFormData);
  }
}

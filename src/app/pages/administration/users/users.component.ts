import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Location } from "@angular/common";
import { ModalService } from "../../../@theme/components/modal/modal.service";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { BehaviorSubject } from "rxjs";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./users.component.scss"],
})
export class UsersComponent implements OnInit {
  constructor(
    private _location: Location,
    private modalService: ModalService
  ) {}

  searchText: any;
  bodyText: any;

  ngOnInit() {
    this.bodyText = "This text can be updated in modal 1";
  }

  // modal
  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
  backClicked() {
    this._location.back();
  }

  // mouseover && mouseout
  showIt: boolean = false;

  show() {
    this.showIt = true;
    console.log("show");
  }
  hide() {
    this.showIt = false;
    console.log("hide");
  }
}

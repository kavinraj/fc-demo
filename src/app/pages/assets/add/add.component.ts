import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  ViewChild,
  Renderer2,
} from "@angular/core";
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { FormArray } from "@angular/forms";
// import { IAssetData } from "./formData.model";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.scss"],
})
export class AddComponent {
  submitted = false;

  // tslint:disable-next-line: variable-name
  show_areaContent: boolean = false;
  show_areaToggle() {
    /* TODO : click again need to close the dropdown */
    this.show_areaContent = !this.show_areaContent;
  }
  // Host listener
  @HostListener("click", ["$event.target.id"]) onClick(id: any) {
    // select read more
    if (id === "criticality_button") {
      this.show_areaContent = true;
    } else {
      this.show_areaContent = false;
    }
  }

  selectedHero: string | null = "Select a Criticality";
  heroes: Array<String> = ["Critical", "High", "Medium", "Low", "Information"];
  onSelect(hero): void {
    this.selectedHero = hero;
    this.assetData.criticality = hero;
    console.log(hero);
  }

  networkTags: Array<string> = ["Firewall", "Router", "Switch"];
  locationTags: Array<string> = ["Perimeter", "Internal"];

  logProtocols: Array<string> = ["UDP", "TCP"];

  assetTypes: Array<string> = [
    "Firewall",
    "Cloud",
    "Switch",
    "Router",
    "Console",
  ];

  vendors: Array<string> = ["Cisco ASA"];

  fetchMechanismes: Array<string> = ["Automatic", "Manual"];

  recurranceTypes: Array<string> = ["Hour", "2 Hour", "3 Hour"];

  @ViewChild("asset_information", { static: true })
  assetInformation: ElementRef;
  @ViewChild("analysis_information", { static: true })
  analysisInformation: ElementRef;
  @ViewChild("mechanism_information", { static: true })
  mechanismInformation: ElementRef;
  @ViewChild("stepper", { static: true }) stepper: ElementRef;

  assetForm: FormGroup;
  analysisForm: FormGroup;
  registerForm: FormGroup;

  assetData = {
    assetName: "",
    criticality: "",
    networkTag: "",
    locationTag: "",
    securityAnalysis: false,
    optimizationAnalysis: false,
    logAnalysis: false,
    logIpAddress: "",
    logProtocol: "",
    logSysLogPort: "",
    logAPIURL: "",
    assetType: "",
    vendor: "",
    fetchMechanism: "",
    ipAddress: "",
    port: 0,
    userName: "",
    password: "",
    sshPassword: "",
    recurranceType: "",
    intervalValue: 0,
  };

  constructor(
    private router: Router,
    private elRef: ElementRef,
    private renderer: Renderer2,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  createForm() {
    this.assetForm = this.fb.group({
      AssetName: [this.assetData.assetName, Validators.required],
      Criticality: [this.assetData.criticality, Validators.required],
      NetworkTag: [this.assetData.networkTag, Validators.required],
      LocationTag: [this.assetData.locationTag, Validators.required],
    });

    this.analysisForm = this.fb.group({
      SecurityAnalysis: [this.assetData.securityAnalysis],
      OptimizationAnalysis: [
        this.assetData.optimizationAnalysis
      ],
      LogAnalysis: [this.assetData.logAnalysis],
      LogIpAddress: [this.assetData.logIpAddress],
      logProtocol: [this.assetData.logProtocol],
      logSysLogPort: [this.assetData.logSysLogPort],
      logAPIURL: [this.assetData.logAPIURL],
    });

    this.analysisForm.get('LogAnalysis').valueChanges
    .subscribe(value => {
      if(value) {
        this.analysisForm.get('LogIpAddress').setValidators(Validators.required);
        this.analysisForm.get('LogIpAddress').updateValueAndValidity();
        this.analysisForm.get('logProtocol').setValidators(Validators.required);
        this.analysisForm.get('logProtocol').updateValueAndValidity();
        this.analysisForm.get('logSysLogPort').setValidators(Validators.required);
        this.analysisForm.get('logSysLogPort').updateValueAndValidity();
        this.analysisForm.get('logAPIURL').setValidators(Validators.required);
        this.analysisForm.get('logAPIURL').updateValueAndValidity();
      } else {
        this.analysisForm.get('LogIpAddress').clearValidators();
        this.analysisForm.get('LogIpAddress').updateValueAndValidity();
        this.analysisForm.get('logProtocol').clearValidators();
        this.analysisForm.get('logProtocol').updateValueAndValidity();
        this.analysisForm.get('logSysLogPort').clearValidators();
        this.analysisForm.get('logSysLogPort').updateValueAndValidity();
        this.analysisForm.get('logAPIURL').clearValidators();
        this.analysisForm.get('logAPIURL').updateValueAndValidity();
      }
    });

    this.registerForm = this.fb.group({
      AssetType: [this.assetData.assetType, Validators.required],
      Vendor: [this.assetData.vendor, Validators.required],
      FetchMechanism: [this.assetData.fetchMechanism, Validators.required],
      IpAddress: [this.assetData.ipAddress, Validators.required],
      Port: [this.assetData.port, Validators.required],
      UserName: [this.assetData.userName, Validators.required],
      Password: [this.assetData.password, Validators.required],
      SSHPassword: [this.assetData.sshPassword, Validators.required],
      RecurranceType: [this.assetData.recurranceType, Validators.required],
      IntervalValue: [this.assetData.intervalValue, Validators.required],
    });
  }


  //get registerFormControl() {
  //  return this.profileForm.controls;
  //}

  // step 1 to step 2
  assetInformationAction($event: any) {
    this.assetInformation.nativeElement.classList.remove("active");
    this.analysisInformation.nativeElement.classList.add("active");
    this.stepper.nativeElement.classList.add("completed");
    this.stepper.nativeElement.nextElementSibling.classList.add("active");
  }
  // step 2 : back
  analysisInformationPrev($event: any) {
    this.analysisInformation.nativeElement.classList.remove("active");
    this.assetInformation.nativeElement.classList.add("active");
  }

  // step 2 to step 3
  analysisInformationNext($event: any) {
    this.analysisInformation.nativeElement.classList.remove("active");
    this.mechanismInformation.nativeElement.classList.add("active");
    // this.stepper.nativeElement.classList.add('completed');
    // this.stepper.nativeElement.nextElementSibling.classList.add('active');
  }

  // step 3 : back
  mechanismInformationPrev($event: any) {
    this.analysisInformation.nativeElement.classList.add("active");
    this.mechanismInformation.nativeElement.classList.remove("active");
  }

  onSubmit() {
    this.submitted = true;
    // TODO: Use EventEmitter with form value
    //console.warn(this.assetForm.value);
    //console.warn(this.analysisForm.value);
    //console.warn(this.registerForm.value);
    alert("SUCCESS!! :-)\n\n" + JSON.stringify(this.assetData, null, 4));
    console.warn(this.assetData);
  }

  iconClose() {
    this.router.navigate(["/assets"]);
  }
}

export class IAssetData {
  assetName: String;
  criticality: String;
  networkTag: String;
  locationTag: String;
  logAnalysis: boolean;
  securityAnalysis: boolean;
  optimizationAnalysis: boolean;
  logIpAddress: String;
  logProtocol: String;
  logSysLogPort: String;
  logAPIURL: String;
  assetType: String;
  vendor: String;
  fetchMechanism: String;
  ipAddress: String;
  port: number;
  userName: String;
  password: String;
  sshPassword: String;
  recurranceType: String;
  intervalValue: number;
}

import {
  Component,
  OnInit,
  ViewEncapsulation,
  HostListener,
  ElementRef,
} from '@angular/core';
import { trigger } from '@angular/animations';
import { Router } from '@angular/router';
import { fadeIn } from '../../@core/animation/animation';
import { AssetList } from '../../@core/data/assets-list';

@Component({
  selector: 'app-assets',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.scss'],
  animations: [trigger('fadeIn', fadeIn())],
  providers: [AssetList],
})
export class AssetsComponent implements OnInit {
  constructor(
    private _element: ElementRef,
    private router: Router,
    private AssetList: AssetList
  ) {}

  // page title
  pageTitle: string | null = 'Assets';
  readMores: boolean = false;

  // Company list
  selectedCompany: string | null = 'Select Company';
  companies: Array<string> = ['Rillusion', 'Aspira', 'Firesec', 'NIA'];
  onSelects(company): void {
    this.selectedCompany = company;
    console.log(company);
  }
  selectcompanyContent: boolean = false;
  selectcompanyToggle(e) {
    /* TODO : click again need to close the dropdown */
    this.selectcompanyContent = !this.selectcompanyContent;
  }

  // select area from company list
  //asset type for filter the data
  selectedCompany_area: string | null = 'Select Area';
  company_areas: Array<string> = ['Demo', 'Sales', 'Creative', 'Development'];
  onCompany_area(company_area): void {
    this.selectedCompany_area = company_area;
    console.log(company_area);
  }
  selectedCompany_areaContent: boolean = false;
  showCompany_areaToggle(e) {
    /* TODO : click again need to close the dropdown */
    this.selectedCompany_areaContent = !this.selectedCompany_areaContent;
  }

  // search
  searchText: any;
  empty: boolean = false;

  // sort by
  sortAsset: string | null = 'Firewall';
  sortAssetLists: Array<string> = [
    'Firewall', 'Cloud', 'Switch', 'Router', 'Console'
  ];
  onSortAssetList(sortAssetList): void {
    this.sortAsset = sortAssetList;
    console.log(sortAssetList);
  }
  sortAssetContent: boolean = false;
  sortAssetContentToggle(e) {
    console.log('toggle: ' + e.type);
    this.sortAssetContent = !this.sortAssetContent;
  }

  // asset list json
  assets: any;

  // asset list card read more
  readmoreContent: boolean[] = [];

  fireEnter(e, i) {
    console.log('type fireEnter: ' + e.type + i);
    this.readmoreContent[i] = !this.readmoreContent[i];
  }
  fireLeave(e, i) {
    console.log('type fireLeave: ' + e.type + i);
    this.readmoreContent[i] = !this.readmoreContent[i];
  }


  toggle__read_more(e, i) {
    console.log('type toggle__read_more: ' + e.type + i);
    // this.readmoreContent = true;
    this.readmoreContent[i] = !this.readmoreContent[i];
  }


  // pagination
  totalItems: number;
  noResults: boolean;
  itemsPerPage: number = 4;
  p: number = 1;

  // totalRow_show: boolean = false;
  totalRow_show: number | null = 4;
  totalRows: Array<number> = [4,8,16,24,32,50];
  ontotalRow(totalRow): void {
    this.totalRow_show = totalRow;
    console.log(totalRow);
  }
  totalRowContent: boolean = false;
  totalRowToggle(e) {
    console.log('toggle: ' + e.type);
    /* TODO : click again need to close the dropdown */
    this.totalRowContent = !this.totalRowContent;
  }

  // Host listener
  @HostListener('click', ['$event.target.id']) onClick(id: any) {
    // select company
    if (id === 'selectedCompany') {
      this.selectcompanyContent = true;
    } else {
      this.selectcompanyContent = false;
    }

    // select area
    if (id === 'selectedCompanyArea') {
      this.selectedCompany_areaContent = true;
    } else {
      this.selectedCompany_areaContent = false;
    }

    // sort by
    if (id === 'sortAssetContentShow') {
      this.sortAssetContent = true;
    } else {
      this.sortAssetContent = false;
    }

    // pagination
    if (id === 'totalRow_show') {
      this.totalRowContent = true;
    } else {
      this.totalRowContent = false;
    }
  }

  dataLoaded = false;
  repodata: [];

  ngOnInit() {
    this.AssetList.fetchData().subscribe((res: { repodata: [] }) => {
      this.repodata = res.repodata;
      this.dataLoaded = true;
      this.totalItems = 50;
    });
  }
}

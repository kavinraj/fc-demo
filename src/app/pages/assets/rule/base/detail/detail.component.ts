import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"],
})
export class RuleDetailComponent implements OnInit {
  // date picker
  matDatepicker: Date;
  picker: any;
  // tab
  viewMode = "rulereview";

  constructor(private _location: Location) { }

  ngOnInit() { }

  backClicked() {
    this._location.back();
  }
}

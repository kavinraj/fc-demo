import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Location } from "@angular/common";

@Component({
  selector: "app-firewall",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./firewall.component.html",
  styleUrls: ["./firewall.component.scss"],
})
export class FirewallComponent implements OnInit {
  constructor(private _location: Location) {}

  ngOnInit() {}

  backClicked() {
    this._location.back();
  }

  searchTextHosts: any;
  searchTextHostsGroup: any;
  searchTextServices: any;
  searchTextServicesGroup: any;
  searchTextInterface: any;
  searchTextZones: any;

     // sort by
  sortAsset: string | null = 'Used Objects';
  sortAssetLists: Array<string> = [
    'Used Objects', 'Unused Objects'
  ];
  onSortAssetList(sortAssetList): void {
    this.sortAsset = sortAssetList;
    console.log(sortAssetList);
  }
  sortAssetContent: boolean = false;
  sortAssetContentToggle(e) {
    console.log('toggle: ' + e.type);
    this.sortAssetContent = !this.sortAssetContent;
  }


}

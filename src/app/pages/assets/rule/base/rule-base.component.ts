import { Component, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { trigger } from '@angular/animations';
import { Location } from '@angular/common';
import { fadeIn } from '../../../../@core/animation/animation';
import { ModalService } from '../../../../@theme/components/modal/modal.service';
import { RuleBase } from '../../../../@core/data/assets-rule-base';

@Component({
  selector: 'app-rule-base',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './rule-base.component.html',
  styleUrls: ['./rule-base.component.scss'],
  animations: [trigger('fadeIn', fadeIn())],
  providers: [RuleBase],
})
export class RuleBaseComponent implements OnInit {

  constructor(private _location: Location,
    private RuleBase: RuleBase, private modalService: ModalService) { }

  // page title
  pageTitle: string | null = 'Rule Base';
  // search
  searchText: any;
  // tab
  viewModeSourceWhereUsed = 'source-where-used-whereGroups';
  viewModeDestinationWhereUsed = 'destination-where-used-whereGroups';
  viewModeServiceWhereUsed = 'service-where-used-whereGroups';

  // load data
  RuleBaseDataLoaded = false;
  RuleBaseData: [];

  ngOnInit() {
    this.RuleBase.fetchData().subscribe((res: { RuleBaseData: [] }) => {
      this.RuleBaseData = res.RuleBaseData;
      this.totalItems = this.RuleBaseData.length;
      this.RuleBaseDataLoaded = true;
      console.log(this.RuleBaseData);
    });
  }

  // pagination
  totalItems: number;
  noResults: boolean;
  itemsPerPage: number = 4;
  p: number = 1;

  // totalRow_show: boolean = false;
  totalRow_show: number | null = 4;
  totalRows: Array<string> = ['4', '8', '16', '24', '32', '50'];
  ontotalRow(totalRow): void {
    this.totalRow_show = totalRow;
    console.log(totalRow);
  }
  totalRowContent: boolean = false;
  totalRowToggle(e) {
    console.log('toggle: ' + e.type);
    /* TODO : click again need to close the dropdown */
    this.totalRowContent = !this.totalRowContent;
  }

  // Host listener
  @HostListener('click', ['$event.target.id']) onClick(id: any) {
    // pagination
    if (id === 'totalRow_show') {
      this.totalRowContent = true;
    } else {
      this.totalRowContent = false;
    }
  }

  // toggle button at top
  ruleBaseCompareCount: number = 0;
  ruleBaseCheckMe(event: any) {
    const ruleBaseFilterExport = document.getElementById('ruleBase-search-filter-export'),
      ruleBaseReviewException = document.getElementById('ruleBase-review-exception');
    if (event.target.checked) {
      this.ruleBaseCompareCount++;
    }
    else {
      this.ruleBaseCompareCount--;
    }
    if (this.ruleBaseCompareCount >= 1) {
      ruleBaseFilterExport.hidden = true;
      ruleBaseReviewException.hidden = false;
    } else {
      ruleBaseFilterExport.hidden = false;
      ruleBaseReviewException.hidden = true;
    }
  }

  // modal
  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

}

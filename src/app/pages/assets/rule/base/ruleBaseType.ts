export class RuleBaseType {

  constructor(
    public assetName = '',
    public criticality = '',
    public networkTag = '',
    public analysis = '',
    public ipAddress = '',
    public assetType = '',
    public vendorType = '',
    public fetchmechanism = '',

  ) { }
}

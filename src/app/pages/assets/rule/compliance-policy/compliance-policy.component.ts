import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";

@Component({
  selector: "app-compliance-policy",
  templateUrl: "./compliance-policy.component.html",
  styleUrls: ["./compliance-policy.component.scss"],
})
export class CompliancePolicyComponent implements OnInit {
  constructor(private _location: Location) {}

  ngOnInit() {}
  searchText: any;

  backClicked() {
    this._location.back();
  }
  sortType: boolean = false;
  onSortBy_value: string | null = "Date Created";
  sortBys: Array<string> = ["Modified Date", "Organization", "Created By"];

  onSortBy(sortBy: any): void {
    this.onSortBy_value = sortBy;
    console.log(sortBy);
  }
}

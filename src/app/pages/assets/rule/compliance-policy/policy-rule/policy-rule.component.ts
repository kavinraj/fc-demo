import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";

@Component({
  selector: "app-policy-rule",
  templateUrl: "./policy-rule.component.html",
  styleUrls: ["./policy-rule.component.scss"],
})
export class PolicyRuleComponent implements OnInit {
  constructor(private _location: Location) {}

  ngOnInit() {}
  searchText: any;

  backClicked() {
    this._location.back();
  }
  sortType: boolean = false;
  onSortBy_value: string | null = "Date Created";
  sortBys: Array<string> = ["Modified Date", "Organization", "Created By"];

  onSortBy(sortBy: any): void {
    this.onSortBy_value = sortBy;
    console.log(sortBy);
  }
}

import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
} from "@angular/core";
import { Location } from "@angular/common";
import { ModalService } from "../../../../@theme/components/modal/modal.service";
import { DiffEditorModel } from "ngx-monaco-editor";

@Component({
  selector: "app-overview",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./overview.component.html",
  styleUrls: ["./overview.component.scss"],
})
export class OverviewComponent implements OnInit {
  searchText: any;
  bodyText: any;

  constructor(
    private _location: Location,
    private modalService: ModalService
  ) {}

  ngOnInit() {
    this.bodyText = "This text can be updated in modal 1";
  }

  assetInformationAction: any;

  // modal
  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
  backClicked() {
    this._location.back();
  }

  // raw config
  code: string = `
  <div id="app">

  <div class="box content">

    <h1>Filtering</h1>
    <p>Update the message in the text box below, and the filters will be automatically applied. Filters are applied using the pipe separator in the mustache interpolations (<code v-text="'{{ input | filter }}'"></code>) and <code>v-bind</code> expressions. Multiple filters can be chained together with additional pipe separators (<code v-text="'{{ input | filter | anotherFilter }}'"></code>).</p>

    <label class="input-box">
      <div>Input:</div>
      <input type="text" v-model="textInput" />
    </label>

    <div class="examples">

      <div class="example">
        Uppercase:
        <pre> {{ textInput | uppercase }} </pre>
      </div>
<div class="example">
        Lowercase:
        <pre> {{ textInput | lowercase }} </pre>
      </div>

      <div class="example">
        Titlecase:
        <pre> {{ textInput | titlecase }} </pre>
      </div>



      <div class="example">
        Reverse:
        <pre> {{ textInput | reverse }} </pre>
      </div>

      <div class="example">
        Pig Latin:
        <pre> {{ textInput | piglatin }} </pre>
      </div>


      <div class="example">
        Reverse Piglatin:
        <pre> {{ textInput | piglatin | reverse }} </pre>
      </div>

    </div>

  </div>
</div>
`;

  options = {
    theme: "vs",
    readOnly: true,
    contextmenu: false,
    minimap: {
      enabled: false,
    },
    renderSideBySide: true,
    selectOnLineNumbers: true,
    wrappingIndent: "on",
    wordWrap: "on",
    fontSize: 14,
    fontFamily: "RobotoMono",
    automaticLayout: true,
    scrollBeyondLastLine: false,
    language: "javascript",
  };
}

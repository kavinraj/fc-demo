import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Location } from "@angular/common";

@Component({
  selector: "app-generate",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./generate.component.html",
  styleUrls: ["./generate.component.scss"],
})
export class GenerateComponent implements OnInit {
  constructor(private _location: Location) {}

  ngOnInit() {}

  backClicked() {
    this._location.back();
  }

  searchText: any;
  sortType: boolean = false;
  onSortBy_value: string | null = "Used Objects";
  sortBys: Array<string> = ["Host"];

  onSortBy(sortBy: any): void {
    this.onSortBy_value = sortBy;
    console.log(sortBy);
  }
}

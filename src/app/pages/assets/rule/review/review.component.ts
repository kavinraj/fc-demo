import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-review',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  viewMode = 'assignedRules';
  searchText: string;
  hideRuleContent: boolean[] = [];
  toggle(i) {
    // toggle based on index
    // index need to add from for loop
    this.hideRuleContent[i] = !this.hideRuleContent[i];
  }

  COMPARE_COUNT: number = 0;
  checkme(event: any) {

    const filterexport = document.getElementById("search-filter-export"),
      linkreview = document.getElementById("rule-link-review");

    if (event.target.checked) {
      this.COMPARE_COUNT++;
    }
    else {
      this.COMPARE_COUNT--;
    }

    if (this.COMPARE_COUNT == 1) {
      filterexport.hidden = true;
      linkreview.hidden = false;
    } else {
      filterexport.hidden = false;
      linkreview.hidden = true;
    }

  }

}

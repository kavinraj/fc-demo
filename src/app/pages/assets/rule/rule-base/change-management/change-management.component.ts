import { Component, OnInit, ViewEncapsulation } from "@angular/core";

@Component({
  selector: "app-change-management",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./change-management.component.html",
  styleUrls: ["./change-management.component.scss"],
})
export class ChangeManagementComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  viewMode = "nominatedrule";
  searchText: string;
  hideRuleContent: boolean[] = [];
  toggle(i) {
    // toggle based on index
    // index need to add from for loop
    this.hideRuleContent[i] = !this.hideRuleContent[i];
  }

  COMPARE_COUNT: number = 0;
  checkme(event: any) {
    const filterexport = document.getElementById("search-filter-export"),
      linkreview = document.getElementById("rule-link-review");

    if (event.target.checked) {
      this.COMPARE_COUNT++;
    } else {
      this.COMPARE_COUNT--;
    }

    if (this.COMPARE_COUNT == 1) {
      filterexport.hidden = true;
      linkreview.hidden = false;
    } else {
      filterexport.hidden = false;
      linkreview.hidden = true;
    }
  }
}

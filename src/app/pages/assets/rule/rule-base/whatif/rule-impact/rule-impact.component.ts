import { Component, OnInit } from "@angular/core";
import * as echarts from "echarts";

@Component({
  selector: "app-rule-impact",
  templateUrl: "./rule-impact.component.html",
  styleUrls: ["./rule-impact.component.scss"],
})
export class RuleImpactComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    const myChart: any = echarts.init(
      <HTMLInputElement>document.getElementById("chartdonut")
    );
    const myChart2: any = echarts.init(
      <HTMLInputElement>document.getElementById("chartdonut2")
    );
    // specify chart configuration item and data
    const chart_option: any = {
      tooltip: {},
      legend: {
        orient: "vertical",
        bottom: "-60px",
        data: [
          "Insecure rules",
          "Insecure service",
          "Redundant rules",
          "Shadow rules",
          "Disabled",
        ],
      },
      series: [
        {
          name: "Before Evaluation",
          type: "pie",
          radius: ["50%", "80%"],
          avoidLabelOverlap: true,
          label: {
            normal: {
              show: false,
              position: "center",
            },
            emphasis: {
              show: false,
              textStyle: {
                fontSize: "30",
                fontWeight: "bold",
              },
            },
          },
          labelLine: {
            normal: {
              show: false,
            },
          },
          data: [
            { value: 335, name: "Insecure rules" },
            { value: 310, name: "Insecure service" },
            { value: 234, name: "Redundant rules" },
            { value: 135, name: "Shadow rules" },
            { value: 1548, name: "Disabled" },
          ],
        },
      ],
    };
    const chart_option2: any = {
      tooltip: {},
      legend: {
        orient: "horizontal",
        bottom: "0",
        data: [
          "Insecure rules 2",
          "Insecure service 2",
          "Redundant rules 2",
          "Shadow rules 2",
          "Disabled 2",
        ],
      },
      series: [
        {
          name: "After Evaluation",
          type: "pie",
          radius: ["50%", "80%"],
          avoidLabelOverlap: true,
          label: {
            normal: {
              show: false,
            },
            emphasis: {
              show: false,
            },
          },
          labelLine: {
            normal: {
              show: false,
            },
          },
          data: [
            { value: 335, name: "Insecure 2 rules" },
            { value: 310, name: "Insecure 2 service" },
            { value: 234, name: "Redundant 2 rules" },
            { value: 135, name: "Shadow 2 rules" },
            { value: 1548, name: "Disabled 2" },
          ],
        },
      ],
    };
    // use configuration item and data specified to show chart
    myChart.setOption(chart_option);
    myChart2.setOption(chart_option2);
  }
}

import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";

@Component({
  selector: "app-whatif",
  templateUrl: "./whatif.component.html",
  styleUrls: ["./whatif.component.scss"],
})
export class WhatifComponent implements OnInit {
  constructor(private _location: Location) {}

  ngOnInit() {}
  searchText: any;

  backClicked() {
    this._location.back();
  }

  COMPARE_COUNT: number = 0;
  checkme(event: any) {
    const filterexport = document.getElementById("search-filter-export"),
      linkreview = document.getElementById("rule-link-review");

    if (event.target.checked) {
      this.COMPARE_COUNT++;
      console.log(this.COMPARE_COUNT);
    } else {
      this.COMPARE_COUNT--;
      console.log(this.COMPARE_COUNT);
    }

    if (this.COMPARE_COUNT == 1) {
      filterexport.hidden = true;
      linkreview.hidden = false;
    } else {
      filterexport.hidden = false;
      linkreview.hidden = true;
    }
  }
}

import { Component, OnInit,
  HostListener,
  ViewEncapsulation} from "@angular/core";
  import { trigger } from '@angular/animations';
  import { fadeIn } from '../../../../@core/animation/animation';
  import { ModalService } from '../../../../@theme/components/modal/modal.service';
  import { RuleBase } from '../../../../@core/data/assets-rule-base';

@Component({
  selector: "app-rule-security-analysis",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./rule-security-analysis.component.html",
  styleUrls: ["./rule-security-analysis.component.scss"],
  animations: [trigger('fadeIn', fadeIn())],
  providers: [RuleBase],
})
export class RuleSecurityAnalysisComponent implements OnInit {
  //page title
  pageTitle: string | null = 'Security Analysis ';
// search
  searchText: any;

  // tab
  viewModeSourceWhereUsed = 'source-where-used-whereGroups';
  viewModeDestinationWhereUsed = 'destination-where-used-whereGroups';
  viewModeServiceWhereUsed = 'service-where-used-whereGroups';

  //asset type for filter the data
  sortAsset: string | null = 'Overly Permissive';
  sortAssetLists: Array<string> = ["Overly Permissive",
  "Any Source",
  "Any Destination",
  "Any Service",
  "Any Source Destination",
  "Any Source Service",
  "Any Destination Service",
  "Logging Disabled",
  "Drop Rules Logging Disabled",
  "More Ports Access",
  "Inactive",
  "Risky Ports",
  "Symmetric Rule",
  "Too Many IPs in Destination",
  ];

  onSortAssetList(sortAssetList): void {
    this.sortAsset = sortAssetList;
    console.log(sortAssetList);
  }
  sortAssetContent: boolean = false;
  sortAssetContentToggle(e) {
    console.log('toggle: ' + e.type);
    this.sortAssetContent = !this.sortAssetContent;
  }

  // pagination
  totalItems: number;
  noResults: boolean;
  itemsPerPage: number = 4;
  p: number = 1;

  // totalRow_show: boolean = false;
  totalRow_show: number | null = 4;
  totalRows: Array<string> = ['4', '8', '16', '24', '32', '50'];
  ontotalRow(totalRow): void {
    this.totalRow_show = totalRow;
    console.log(totalRow);
  }
  totalRowContent: boolean = false;
  totalRowToggle(e) {
    console.log('toggle: ' + e.type);
    /* TODO : click again need to close the dropdown */
    this.totalRowContent = !this.totalRowContent;
  }

  @HostListener('click', ['$event.target.id']) onClick(id: any) {
    // select by
    if (id === 'sortAssetContentShow') {
      this.sortAssetContent = true;
    } else {
      this.sortAssetContent = false;
    }

    // pagination
    if (id === 'totalRow_show') {
      this.totalRowContent = true;
    } else {
      this.totalRowContent = false;
    }
  }


  constructor(
    private RuleBase: RuleBase, private modalService: ModalService) {}

  // modal
  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
  // load data
  RuleBaseDataLoaded = false;
  RuleBaseData: [];

  ngOnInit() {
    this.RuleBase.fetchData().subscribe((res: { RuleBaseData: [] }) => {
      this.RuleBaseData = res.RuleBaseData;
      this.totalItems = this.RuleBaseData.length;
      this.RuleBaseDataLoaded = true;
      console.log(this.RuleBaseData);
    });
  }
}

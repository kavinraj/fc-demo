import {
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { trigger } from '@angular/animations';
import { fadeIn } from '../../../@core/animation/animation';
import { AssetView } from '../../../@core/data/assets-view';

@Component({
  selector: 'app-rule',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.scss'],
  animations: [trigger('fadeIn', fadeIn())],
  providers: [AssetView],
})
export class RuleComponent implements OnInit {

  state$: Observable<any>;

  constructor(private activatedRoute: ActivatedRoute,
    private AssetView: AssetView) {}

  AssetViewDataLoaded = false;
  AssetViewData: [];

  demo: number | null = 1;

  ngOnInit() {

    this.state$ = this.activatedRoute.paramMap.pipe(
      map(() => window.history.state.id)
    );

    this.demo = window.history.state.id;
    console.log(this.demo);

    this.AssetView.fetchData().subscribe((res: { AssetViewData: [] }) => {
      this.AssetViewData = res.AssetViewData;
      this.AssetViewDataLoaded = true;
      console.log(this.AssetViewData);
    });
  }

}

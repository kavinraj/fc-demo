import {
  Component,
  ViewEncapsulation,
  Input,
  Output,
  OnInit
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DiffEditorModel } from 'ngx-monaco-editor';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-compare',
  templateUrl: './view-compare.component.html',
  styleUrls: ['./view-compare.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ViewCompareComponent implements OnInit {
  constructor(private _location: Location) {}

  backClicked() {
    this._location.back();
  }

  text1 = `import { Component, OnInit, Input } from '@angular/core';

  @Component({
    selector: 'app-tooltip',
    templateUrl: './tooltip.component.html',
    styleUrls: ['./tooltip.component.css']
  })
  export class TooltipComponent implements OnInit {
    @Input() tooltipDirection: string;
    @Input() title: string;
    @Input() tooltipData: string;
    constructor() { }

    ngOnInit() {
    }

  }`;
  text2 = `import { Component, OnInit, Input } from '@angular/core';

  @Component({
    selector: 'app-tooltip',
    templateUrl: './tooltip.component.html',
    styleUrls: ['./tooltip.component.css']
  })
  export class TooltipComponent implements OnInit {
    @Input() tooltipDirection: <Array>;
    constructor() { }

    ngOnInit() {
    }

  }`;

  isCompared = true;

  // compare, output
  diffOptions = {
    theme: 'vs',
    language: 'typescript',
    readOnly: true,
    contextmenu: false,
    minimap: {
      enabled: false
    },
    renderSideBySide: true,
    selectOnLineNumbers: true,
    wrappingIndent: 'on',
    wordWrap: 'on',
    fontSize: 14,
    fontFamily: 'RobotoMono',
    automaticLayout: true,
    scrollBeyondLastLine: false
  };
  originalModel: DiffEditorModel = {
    code: '',
    language: 'typescript'
  };

  modifiedModel: DiffEditorModel = {
    code: '',
    language: 'typescript'
  };

  public ngOnInit() {
    this.originalModel = Object.assign({}, this.originalModel, {
      code: this.text1
    });
    this.modifiedModel = Object.assign({}, this.originalModel, {
      code: this.text2
    });
    this.isCompared = true;
    window.scrollTo(0, 0); // scroll the window to top
  }

  onChangeInline($event) {
    const checked = $event.target.checked;
    if ($event.target.id === 'test1') {
      console.log(checked);
      this.diffOptions = Object.assign({}, this.diffOptions, {
        renderSideBySide: checked
      });
    } else {
      this.diffOptions = Object.assign({}, this.diffOptions, {
        renderSideBySide: !checked
      });
    }
  }
}

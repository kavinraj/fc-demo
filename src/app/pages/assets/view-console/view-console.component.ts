import { Component, OnInit, HostListener, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-console',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './view-console.component.html',
  styleUrls: ['./view-console.component.scss']
})
export class ViewConsoleComponent implements OnInit {

  // page title
  pageTitle: string = 'Management Console - Friday';

  // items in page to show
  itemsPerPage: number = 4;

  // search
  searchText: string;

  // sort by
  sortAsset: string | null = 'Firewall';
  sortAssetLists: Array<string> = [
    'Total Rules', 'Log', 'Security', 'Parsing', 'Mechanism'
  ];
  onSortAssetList(sortAssetList): void {
    this.sortAsset = sortAssetList;
    console.log(sortAssetList);
  }
  sortAssetContent: boolean = false;
  sortAssetContentToggle(e) {
    console.log('toggle: ' + e.type);
    this.sortAssetContent = !this.sortAssetContent;
  }

  // asset list json
  repo: any;

  // asset list card read more
  fireEvent(e) {
    console.log(e.type);
  }
  readmoreContent = false;
  toggle__read_more(e) {
    console.log(e.type);
    this.readmoreContent = true;
  }
  fireOut(e) {
    console.log(e);
    this.readmoreContent = false;
  }

  // Host listener
  @HostListener('click', ['$event.target.id']) onClick(id: any) {
    // sort by
    if (id === 'sortAssetContentShow') {
      this.sortAssetContent = true;
    } else {
      this.sortAssetContent = false;
    }

  }
  constructor(private _location: Location) { }

  ngOnInit() {
  }

  backClicked() {
    this._location.back();
  }
}

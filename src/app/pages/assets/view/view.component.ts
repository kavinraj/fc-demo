import {
  Component,
  OnInit,
  HostListener,
  ViewEncapsulation,
  ElementRef,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { trigger } from '@angular/animations';
import { fadeIn } from '../../../@core/animation/animation';
import { AssetView } from '../../../@core/data/assets-view';
import { ModalService } from '../../../@theme/components/modal/modal.service';

@Component({
  selector: 'app-view',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  animations: [trigger('fadeIn', fadeIn())],
  providers: [AssetView],
})
export class ViewComponent implements OnInit {
  go(event, items) {
    // console.log("Checking passed item: ",event);
    // console.log("Checking passed item: ",items);
    this.router.navigateByUrl('/assets/rule/overview', { state: { id: items } });
  }
  //asset type for filter the data
  searchText: string;
  sortType: boolean = false;
  onSortBy_value: string | null = 'Date';
  sortBys: Array<string> = [
    'Total Rules',
    'Log',
    'Security',
    'Parsing',
    'Mechanism',
  ];

  onSortBy(sortBy: any): void {
    this.onSortBy_value = sortBy;
    console.log(sortBy);
  }

  assetViewCompareCount: number = 0;
  assetViewCheckMe(event: any) {
    const fileCompareButton = document.getElementById('fileCompareButton'),
      addAssetButton = document.getElementById('uploadAssetButton');

    if (event.target.checked) {
      this.assetViewCompareCount++;
    } else {
      this.assetViewCompareCount--;
    }

    if (this.assetViewCompareCount == 2) {
      fileCompareButton.classList.remove('d-none');
      addAssetButton.classList.add('d-none');
    } else {
      fileCompareButton.classList.add('d-none');
      addAssetButton.classList.remove('d-none');
    }
  }

  constructor(
    private _element: ElementRef,
    private router: Router,
    private modalService: ModalService,
    private http: HttpClient,
    private AssetView: AssetView
  ) { }


  // modal
  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  // sort by
  sortAsset: string | null = 'Firewall';
  sortAssetLists: Array<string> = [
    'Total Rules', 'Log', 'Security', 'Parsing', 'Mechanism'
  ];
  onSortAssetList(sortAssetList): void {
    this.sortAsset = sortAssetList;
    console.log(sortAssetList);
  }
  sortAssetContent: boolean = false;
  sortAssetContentToggle(e) {
    console.log('toggle: ' + e.type);
    this.sortAssetContent = !this.sortAssetContent;
  }

  // Host listener
  @HostListener('click', ['$event.target.id']) onClick(id: any) {
    // sort by
    if (id === 'sortAssetContentShow') {
      this.sortAssetContent = true;
    } else {
      this.sortAssetContent = false;
    }

  }

  AssetViewDataLoaded = false;
  AssetViewData: [];

  ngOnInit() {
    this.AssetView.fetchData().subscribe((res: { AssetViewData: [] }) => {
      this.AssetViewData = res.AssetViewData;
      this.AssetViewDataLoaded = true;
      // console.log(this.AssetViewData);
    });
  }
  compareConfig() {
    this.router.navigate(['/assets/view/compare']);
  }

  // fileUpload
  uploadedFiles: Array<File>;
  fileChange(element) {
    this.uploadedFiles = element.target.files;
  }

  upload() {
    let formData = new FormData();
    for (var i = 0; i < this.uploadedFiles.length; i++) {
      formData.append("uploads[]", this.uploadedFiles[i], this.uploadedFiles[i].name);
    }
    this.http.post('<</api/upload>>', formData)
      .subscribe((response) => {
        console.log('response received is ', response);
      })
  }
}


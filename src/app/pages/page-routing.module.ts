import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { TopologyComponent } from './topology/topology.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrganizationComponent } from './organization/organization.component';

import { AssetsComponent } from './assets/assets.component';
import { AddComponent } from './assets/add/add.component';
import { ViewComponent } from './assets/view/view.component';
import { ViewCompareComponent } from './assets/view-compare/view-compare.component';
import { ViewConsoleComponent } from './assets/view-console/view-console.component';

import { RuleComponent } from './assets/rule/rule.component';
import { OverviewComponent } from './assets/rule/overview/overview.component';
import { RuleBaseComponent } from './assets/rule/base/rule-base.component';
import { FirewallComponent } from './assets/rule/base/firewall/firewall.component';
import { RuleDetailComponent } from './assets/rule/base/detail/detail.component';
import { RuleSecurityAnalysisComponent } from './assets/rule/rule-security-analysis/rule-security-analysis.component';
import { ShadowRulesComponent } from './assets/optimization/shadow-rules/shadow-rules.component';
import { RedundantRulesComponent } from './assets/optimization/redundant-rules/redundant-rules.component';
import { SourceGroupingComponent } from './assets/optimization/source-grouping/source-grouping.component';
import { DestinationGroupingComponent } from './assets/optimization/destination-grouping/destination-grouping.component';
import { ServiceGroupingComponent } from './assets/optimization/service-grouping/service-grouping.component';
import { LargeSubnetsComponent } from './assets/optimization/large-subnets/large-subnets.component';
import { UnusedObjectsComponent } from './assets/optimization/unused-objects/unused-objects.component';


const pageRoutes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/dashboard' },
  { path: 'topology', component: TopologyComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'organization', component: OrganizationComponent },
  {
    path: 'assets',
    children: [
      {
        path: '',
        component: AssetsComponent,
      },
      {
        path: 'add',
        component: AddComponent
      },
      {
        path: 'view',
        children: [
          {
            path: '',
            component: ViewComponent,
          },
          {
            path: 'compare',
            component: ViewCompareComponent
          },
          {
            path: 'console',
            component: ViewConsoleComponent
          }
        ],
      },
      {
        path: 'rule',
        children: [
          {
            path: '',
            component: RuleComponent,
            children: [
              {
                path: 'overview',
                component: OverviewComponent,
              },
              {
                path: 'base',
                children: [
                  {
                    path: '',
                  component: RuleBaseComponent,
                  },
                  {
                    path: 'firewall/:id',
                    component: FirewallComponent,
                  },
                  {
                    path: 'detail/:id',
                    component: RuleDetailComponent,
                  },
                ]
              },
              {
                path: 'security',
                component: RuleSecurityAnalysisComponent,
              },
              {
                path: 'optimization',
                children: [
                  {
                    path: 'shadow-rule',
                    component: ShadowRulesComponent,
                  },
                  {
                    path: 'redundant-rules',
                    component: RedundantRulesComponent,
                  },
                  {
                    path: 'source-grouping',
                    component: SourceGroupingComponent,
                  },
                  {
                    path: 'destination-grouping',
                    component: DestinationGroupingComponent,
                  },
                  {
                    path: 'service-grouping',
                    component: ServiceGroupingComponent,
                  },
                  {
                    path: 'large-subnets',
                    component: LargeSubnetsComponent,
                  },
                  {
                    path: 'unused-objects',
                    component: UnusedObjectsComponent,
                  },
                ],
              },
            ]
          }
        ]
      }
    ],
  },


];

@NgModule({
  imports: [RouterModule.forChild(pageRoutes)],
  exports: [RouterModule],
})
export class PageRoutingModule { }

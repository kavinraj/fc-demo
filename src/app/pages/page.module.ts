import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThemeModule } from '../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { OverlayModule } from '@angular/cdk/overlay';
import {
  MatTooltipModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatInputModule,
} from '@angular/material';

import { FilterPipe } from '../@theme/pipes/search.pipe';
import { NgxPaginationModule } from 'ngx-pagination';

import { PageRoutingModule } from './page-routing.module';
import { PageLayoutComponent } from './page.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AssetsComponent } from './assets/assets.component';
import { OrganizationComponent } from './organization/organization.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { TopologyComponent } from './topology/topology.component';
import { AddComponent } from './assets/add/add.component';
import { EditComponent } from './assets/edit/edit.component';
import { ViewComponent } from './assets/view/view.component';
import { ViewCompareComponent } from './assets/view-compare/view-compare.component';
import { ViewConsoleComponent } from './assets/view-console/view-console.component';
import { OverviewComponent } from './assets/rule/overview/overview.component';
import { ReviewComponent } from './assets/rule/review/review.component';
import { RuleBaseComponent } from './assets/rule/base/rule-base.component';
import { RuleDetailComponent } from './assets/rule/base/detail/detail.component';
import { RuleSecurityAnalysisComponent } from './assets/rule/rule-security-analysis/rule-security-analysis.component';
import { PolicyComponent } from './assets/policy/policy.component';
import { ClientComponent } from './administration/client/client.component';
import { LicenseComponent } from './administration/license/license.component';
import { SettingComponent } from './administration/setting/setting.component';
import { EventLogComponent } from './administration/event-log/event-log.component';
import { AdminOverviewComponent } from './administration/overview/overview.component';
import { UsersComponent } from './administration/users/users.component';
import { RuleComponent } from './assets/rule/rule.component';
import { AdministrationComponent } from './administration/administration.component';
import { CompliancePolicyComponent } from './assets/rule/compliance-policy/compliance-policy.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { WhatifComponent } from './assets/rule/rule-base/whatif/whatif.component';
import { RuleImpactComponent } from './assets/rule/rule-base/whatif/rule-impact/rule-impact.component';
import { ShadowRuleComponent } from './assets/rule/rule-base/whatif/shadow-rule/shadow-rules.component';
import { ChangeManagementComponent } from './assets/rule/rule-base/change-management/change-management.component';
import { GenerateComponent } from './assets/rule/report/generate/generate.component';
import { FirewallComponent } from './assets/rule/base/firewall/firewall.component';
import { PolicyRuleComponent } from './assets/rule/compliance-policy/policy-rule/policy-rule.component';
import { ShadowRulesComponent } from './assets/optimization/shadow-rules/shadow-rules.component';
import { RedundantRulesComponent } from './assets/optimization/redundant-rules/redundant-rules.component';
import { SourceGroupingComponent } from './assets/optimization/source-grouping/source-grouping.component';
import { DestinationGroupingComponent } from './assets/optimization/destination-grouping/destination-grouping.component';
import { ServiceGroupingComponent } from './assets/optimization/service-grouping/service-grouping.component';
import { LargeSubnetsComponent } from './assets/optimization/large-subnets/large-subnets.component';
import { UnusedObjectsComponent } from './assets/optimization/unused-objects/unused-objects.component';

@NgModule({
  declarations: [
    PageLayoutComponent,
    DashboardComponent,
    AssetsComponent,
    OrganizationComponent,
    NotFoundComponent,
    FilterPipe,
    TopologyComponent,
    ViewComponent,
    ViewCompareComponent,
    AddComponent,
    EditComponent,
    ViewConsoleComponent,
    OverviewComponent,
    ReviewComponent,
    RuleBaseComponent,
    RuleDetailComponent,
    RuleSecurityAnalysisComponent,
    ShadowRulesComponent,
    PolicyComponent,
    ClientComponent,
    LicenseComponent,
    SettingComponent,
    EventLogComponent,
    AdminOverviewComponent,
    UsersComponent,
    RuleComponent,
    AdministrationComponent,
    CompliancePolicyComponent,
    SignupComponent,
    LoginComponent,
    ForgotpasswordComponent,
    WhatifComponent,
    RuleImpactComponent,
    ShadowRuleComponent,
    ChangeManagementComponent,
    GenerateComponent,
    FirewallComponent,
    PolicyRuleComponent,
    RedundantRulesComponent,
    SourceGroupingComponent,
    DestinationGroupingComponent,
    ServiceGroupingComponent,
    LargeSubnetsComponent,
    UnusedObjectsComponent,
  ],
  imports: [
    PageRoutingModule,
    MonacoEditorModule.forRoot(),
    CommonModule,
    ThemeModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    MatNativeDateModule,
    MatTooltipModule,
    MatInputModule,
    MatDatepickerModule,
    OverlayModule,
  ],
})
export class PagesModule {}
